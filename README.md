## Collette Short's README

**Collette Short, Staff Strategy and Operations**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

* [GitLab](https://gitlab.com/cshorty)
* [LinkedIn Profile](https://www.linkedin.com/in/collette-short-pmp)

## About me

* I’m from a western suburb of Chicago, Hinsdale. I moved to St. Paul, MN for college and then Fort Lauderdale, FL for graduate school
* I spent 3 years doing sea turtle surveys in Broward County, Florida. This involved marking new sea turtle nests, assisting stranded turtles, and excavating nests that have hatched
* My husband and I met at a [Guster](https://www.guster.com/) Concert in Fort Lauderdale and enjoy attending concerts in our free time. Our house is covered with concert posters
* I have two young daughters that take up my time outside of work. I am excited about seeing the amazing women they grow up to be
* I enjoy cooking new recipes, hiking, working out, and reading in my downtime

## How you can help me

* I am still fairly new to GitLab, so I will ask a lot of questions. Helping me to answer those questions with links or previous notes will help me get up to speed quickly
* I appreciate all feedback, especially when it is feedback that can help me grow as a person and within my career
* I do my best work when we have an agreed upon deadline and understand the why behind the ask

## My working style

* I love the ease and transparency of working in channels on Slack
* I leverage emoji’s because it is a quick way to acknowledge a message and I think it makes work fun!
* Since I have young kids and a large dog that demands two walks a day, I work around 9am - 5pm Eastern Time. I value my time with the children, so prefer not to check work messages outside of working hours

## What I assume about others

* That everyone’s intentions are positive and we are all working towards shared goals for the company
* That when feedback is shared, it isn’t personal but rather an observation that I could do better

## Communicating with me

* I do well communicating via Slack, but know I will sometimes use emojis to acknowledge that I have seen a message

## Strengths

* I am passionate about what I do and making a difference. 
* I will say I don’t know or ask questions when I do not understand something,  I might need to process the information and come back with more questions
* I will do my best to help someone that asks for help or find the correct person to help them

## Weaknesses

* I have a hard time asking for help when I need it and this has caused me to burn out with work before
* I also have a hard time saying no, because I want to help as much as I can


